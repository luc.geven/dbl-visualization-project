export default size => {
    if (size < 1) {
        return [true];
    }
    const buf = Array(1 << size);
    for (let i = buf.length; i--; ) {
      buf[i] = Array(size);
      for (let j = size; j--; ) buf[i][j] = Boolean(i & (1 << j));
    }
    return buf;
  };