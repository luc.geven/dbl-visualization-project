module.exports = parseToHierarchy;

async function parseToHierarchy({ vertices, edges }) {
  const bfs = (G, n) => {
    const graphResult = {
      name: "graph",
      id: "",
      children: []
    };

    const queue = [];

    const result = G.map(() => {
      return {
        color: "white",
        d: Number.MAX_SAFE_INTEGER,
        p: null,
        children: []
      };
    });
    result[0].color = "gray";
    result[0].d = 0;
    result[0].name = n[0];
    result[0].id = "";

    enqueue(queue, 0);

    for (let j = 0; j < n.length; j++) {
      while (queue.length !== 0) {
        const uIndex = dequeue(queue);
        const uResult = result[uIndex];

        // Get nodes adjacent to u
        for (let i = 0; i < G.length; i++) {
          const vResult = result[i];
          if (G[uIndex][i] != 0 && vResult.color === "white") {
            vResult.color = "gray";
            vResult.d = uResult.d + 1;
            uResult.children.push({
              children: vResult.children,
              name: i,
              id: n[i]
            });
            vResult.p = uIndex;
            enqueue(queue, i);
          }
        }
        uResult.color = "black";
      }

      if (result[j].color === "white") {
        result[j].d = 0;
        result[j].name = j;
        result[j].id = n[j];
        enqueue(queue, j);
      }

      if (result[j].p == null) {
        const { name, children } = result[j];
        graphResult.children.push({ name, children });
      }
    }

    return graphResult;
  };

  const enqueue = (Q, n) => {
    Q.push(n);
  };

  const dequeue = Q => {
    return Q.splice(0, 1);
  };

  const file = bfs(edges, vertices);

  var cache = [];
  return JSON.stringify(file, (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (cache.indexOf(value) !== -1) {
        // Circular reference found, discard key
        return;
      }
      // Store value in our collection
      cache.push(value);
    }
    return value;
  });
}
