export default async ({ vertices: nodes, edges: links }, ascending = true) => {
  const data = nodes.map((v, i) => {
    return { vertex: v, edges: links[i] }
  })

  data.sort((a, b) => {
    if (a.vertex < b.vertex) {
      return ascending ? -1 : 1;
    } else if (a.vertex > b.vertex) {
      return ascending ? 1 : -1;
    } else {
      return 0;
    }
  })
  const vertices = data.map(d => d.vertex)
  const edges = data.map(d => d.edges)
  return { vertices, edges };
}