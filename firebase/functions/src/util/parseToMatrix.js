module.exports = parseToMatrix;

async function parseToMatrix(data) {
  // declare list of vertices
  const vertices = [];
  const edges = [];

  // split file on rows
  // and get first line of file and split on semicolon
  const matrix = data.split("\n");
  const firstRow = matrix[0].split(";");

  // set maximum loop length to the length of the first matrix row,
  // which will be one shorter than all others due to expected input
  const rowLength = firstRow.length;

  // loop over first row, skipping empty first element
  for (let i = 1; i < rowLength; i++) {
    // add element in spot i to the vertices list
    vertices.push(firstRow[i]);
  }

  // loop over remaining rows of file,
  // skipping the last one as it is empty due to expected input
  for (let i = 1; i < rowLength; i++) {
    // declare current row and empty array to store number matrix
    const row = matrix[i].split(";");
    const edgeHolder = [];

    // loop over the elements in row, skipping the first and the last one,
    // as the first one is the vertex and the last one is empty,
    // due to expected input
    for (let i = 1; i < rowLength; i++) {
      // add element to row
      edgeHolder.push(row[i]);
    }

    // add row to number matrix
    edges.push(edgeHolder);
  }

  // return the vertices and number matrix
  return { vertices, edges };
}
