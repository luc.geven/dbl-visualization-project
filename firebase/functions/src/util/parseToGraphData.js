module.exports = parseToGraphData;

async function parseToGraphData({ vertices, edges }) {
  // create new JSON object
  const graph = { nodes: [], links: [] };

  // set maximum loop length to the length of the vertices,
  // as each vertex corresponds to 1 collumn and to 1 row in the matrix
  const rowlength = vertices.length;

  // loop over all vertices
  for (let i = 0; i < rowlength; i++) {
    // add current vertex to json object
    graph.nodes.push({id: i, name: vertices[i], group: 1 });
  }

  // loop over all rows of the matrix
  for (let i = 0; i < rowlength; i++) {
    // loop over all elements in the current row
    for (let j = 0; j < rowlength; j++) {
      // check if vertex has positive value
      if (edges[i][j] > 0) {
        // if so then add edge between vertex i and vertex j
        graph.links.push({
          source: i,
          target: j,
          value: edges[i][j]
        });
      }
    }
  }

  // return the json object
  return graph;
}
