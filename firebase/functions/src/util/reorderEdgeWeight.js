export default async ({ vertices: nodes, edges: links }, out = false, ascending = true) => {

    const data = nodes.map((v, i) => {
        return { vertex: v, edges: out ? links[i] : links.map((l) => l[i]) }
    })

    data.sort((a, b) => {
        const weightA = a.edges.reduce((prev,next) => prev + next, 0)
        const weightB = b.edges.reduce((prev,next) => prev + next, 0)

        if (weightA < weightB) {
            return ascending ? -1 : 1;
        } else if (weightA > weightB) {
            return ascending ? 1 : -1;
        } else {
            return 0;
        }
    })

    const vertices = data.map(d => d.vertex)
    const edges = data.map(d => d.edges)
    return { vertices, edges };
}