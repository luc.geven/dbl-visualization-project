/* eslint-disable promise/always-return */
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
admin.initializeApp();
import path from "path";
import os from "os";
import fs from "fs";
import { promisify } from "util";

import * as parse from "./util/parse";
import * as reorder from "./util/reorder";
import boolcombo from "./util/boolcombo";

const RAW_PREFIX = "raw_";
const GRAPH_PREFIX = "graph_";
const MATRIX_PREFIX = "matrix_";
const MATRIX_ALPHABETICAL = MATRIX_PREFIX + "alphabetical_";
const MATRIX_DEGREE = MATRIX_PREFIX + "degree_";
const MATRIX_EDGEWEIGHT = MATRIX_PREFIX + "edgeweight_";
const HIERARCHY_PREFIX = "hierarchy_";
const JSON_EXTENSION = ".json";

export const parseUploadedFiles = functions
  .region("europe-west1")
  .runWith({ memory: "2GB" })
  .storage.object()
  .onFinalize(async object => {
    const filePath = object.name;
    const fileDir = path.dirname(filePath);
    const baseFileName = path.basename(filePath, path.extname(filePath));
    const tempLocalFile = path.join(os.tmpdir(), baseFileName);
    const tempLocalDir = path.dirname(tempLocalFile);
    const filesizes = {};

    // Exit if the file is not the raw file
    if (!baseFileName.startsWith(RAW_PREFIX)) {
      return console.log("Not a raw file. Returning from function.");
    }

    const bucket = admin.storage().bucket();

    // Download the raw file to the function's temp folder
    await bucket.file(filePath).download({ destination: tempLocalFile });
    console.log("File downloaded locally to", tempLocalFile);

    filesizes[`${RAW_PREFIX}bytes`] = fs.statSync(tempLocalFile).size;

    const readFile = promisify(fs.readFile);
    const writeFile = promisify(fs.writeFile);

    // Read the file that was downloaded to the temp folder
    let contents = await readFile(tempLocalFile, "utf8");

    // Parse the temp file to a matrix
    let matrix = await parse.toMatrix(contents);

    contents = null;

    console.log("Writing matrix file to temp folder", tempLocalDir);
    const matrixFileName = `${baseFileName.replace(RAW_PREFIX, MATRIX_PREFIX)}`;
    const matrixJSONPath = path.format({
      dir: fileDir,
      name: matrixFileName,
      ext: JSON_EXTENSION
    });
    // Write the parsed matrix to the temp folder
    await writeFile(tempLocalFile, JSON.stringify(matrix));

    filesizes[`${MATRIX_PREFIX}bytes`] = fs.statSync(tempLocalFile).size;

    // Add the matrix to Firebase Storage
    await bucket.upload(tempLocalFile, {
      destination: matrixJSONPath,
      resumable: false
    });
    console.log("Matrix JSON uploaded to storage at", matrixJSONPath);

    const orderings = [
      {
        name: MATRIX_ALPHABETICAL,
        ordering: reorder.alphabetically,
        parameters: 1
      },
      { name: MATRIX_DEGREE, ordering: reorder.byDegree, parameters: 2 },
      { name: MATRIX_EDGEWEIGHT, ordering: reorder.byEdgeWeight, parameters: 2 }
    ];

    for (const o of orderings) {
      const params = boolcombo(o.parameters);
      for (const p of params) {
        const orderingResult = await o.ordering(matrix, ...p);

        console.log(
          `Writing matrix ordering ${
            o.name
          } with parameters ${p} to temp folder`,
          tempLocalDir
        );
        const orderingFileName = `${baseFileName.replace(
          RAW_PREFIX,
          o.name + p.map(x => `${x}_`).join("")
        )}`;
        const orderingJSONPath = path.format({
          dir: fileDir,
          name: orderingFileName,
          ext: JSON_EXTENSION
        });
        // Write the parsed ordering to the temp folder
        await writeFile(tempLocalFile, JSON.stringify(orderingResult));

        // Add the ordering to Firebase Storage
        await bucket.upload(tempLocalFile, {
          destination: orderingJSONPath,
          resumable: false
        });
        console.log(
          `Matrix ordering JSON ${
            o.name
          } with parameters ${p} uploaded to storage at`,
          orderingJSONPath
        );
      }
    }

    let hierarchy = await parse.toHierarchy(matrix);

    console.log("Writing hierarchy file to temp folder", tempLocalDir);
    const hierarchyFileName = `${baseFileName.replace(RAW_PREFIX, HIERARCHY_PREFIX)}`;
    const hierarchyJSONPath = path.format({
      dir: fileDir,
      name: hierarchyFileName,
      ext: JSON_EXTENSION
    });
    // Write the parsed graph to the temp folder
    await writeFile(tempLocalFile, hierarchy);

    filesizes[`${HIERARCHY_PREFIX}bytes`] = fs.statSync(tempLocalFile).size;

    // Add the graph to Firebase Storage
    await bucket.upload(tempLocalFile, {
      destination: hierarchyJSONPath,
      resumable: false
    });
    console.log("Hierarchy JSON uploaded to storage at", hierarchyJSONPath);


    let graph = await parse.toGraph(matrix);

    console.log("Writing graph file to temp folder", tempLocalDir);
    const graphFileName = `${baseFileName.replace(RAW_PREFIX, GRAPH_PREFIX)}`;
    const graphJSONPath = path.format({
      dir: fileDir,
      name: graphFileName,
      ext: JSON_EXTENSION
    });
    // Write the parsed graph to the temp folder
    await writeFile(tempLocalFile, JSON.stringify(graph));

    filesizes[`${GRAPH_PREFIX}bytes`] = fs.statSync(tempLocalFile).size;

    // Add the graph to Firebase Storage
    await bucket.upload(tempLocalFile, {
      destination: graphJSONPath,
      resumable: false
    });
    console.log("Graph JSON uploaded to storage at", hierarchyJSONPath);

    matrix = null;
    hierarchy = null;
    graph = null;

    // Set the isParsed flag in Firestore to true
    const doc = admin
      .firestore()
      .collection("datasets")
      .doc(baseFileName.replace(RAW_PREFIX, ""));

    // await doc.update({ isParsed: true });
    // console.log("Changed isParsed flag for uploaded dataset");

    await doc.set({ isParsed: true, ...filesizes }, { merge: true });
    console.log("Set filesizes for uploaded dataset");

    // Release all the temporary files
    return fs.unlinkSync(tempLocalFile);
  });
