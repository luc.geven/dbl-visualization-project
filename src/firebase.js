import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyATg4vLKV0hcOEHIggd68-seT110kbtSEU",
  authDomain: "dbl-hti-webtech.firebaseapp.com",
  databaseURL: "https://dbl-hti-webtech.firebaseio.com",
  projectId: "dbl-hti-webtech",
  storageBucket: "dbl-hti-webtech.appspot.com",
  messagingSenderId: "1009201174550",
  appId: "1:1009201174550:web:1779f9d525d8b8d7"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const storage = firebase.storage();

export { db, storage };
