import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import localforage from "localforage";
import { vuexfireMutations, firestoreAction } from "vuexfire";
import firebase from "firebase/app";

import { db, storage } from "@/firebase";

import axios from "axios";

Vue.use(Vuex);

const vuexPersistPatch = () => {
  return store => {
    store.state.waitForStore = true;
    store.subscribe(mutation => {
      if (mutation.type === "RESTORE_MUTATION") {
        store._vm.$root.$emit("storageReady");
        store.state.waitForStore = false;
      }
    });
  };
};

const vuexLocal = new VuexPersistence({
  storage: localforage,
  strictMode: true,
  asyncStorage: true,
  reducer: state => ({
    visualisation: state.visualisation,
    dark: state.dark,
    currentDataset: {
      id: state.currentDataset.id,
      name: state.currentDataset.name,
      data: {}
    },
    descending: state.descending
  })
});

export default new Vuex.Store({
  plugins: [vuexLocal.plugin, vuexPersistPatch()],
  state: {
    files: [],
    visualisation: {
      left: {
        id: 0,
        requirements: "graph"
      },
      right: {
        id: 0,
        requirements: "matrix",
        type: undefined
      }
    },
    dark: true,
    loadingMessage: null,
    datasetHistory: [],
    currentDataset: {
      id: null,
      data: { matrix: undefined, graph: undefined, hierarchy: undefined }
    },
    selectedNodes: [],
    degree: 1,
    maxDegree: 1,
    zoomSelectionNodes: [],
    zoomSelectionLinks: [],
    isZoomed: false,
    descending: false,
    insights: []
  },
  mutations: {
    ...vuexfireMutations,
    RESTORE_MUTATION: vuexLocal.RESTORE_MUTATION,
    updateFiles(state, files) {
      state.files = files;
    },
    setCurrentDatasetId(state, { id, name }) {
      Vue.set(state.currentDataset, "id", id);
      Vue.set(state.currentDataset, "name", name);
      Vue.set(state.currentDataset, "data", {});
    },
    setCurrentDatasetData(state, { dataType, data }) {
      //Vue.set(state.currentDataset.data, dataType, {});
      Vue.set(state.currentDataset.data, dataType, data);
    },
    setLoadingMessage(state, value) {
      state.loadingMessage = value;
    },
    toggleDark(state) {
      state.dark = !state.dark;
    },
    setVisType(state, { side, id, requirements, type }) {
      Vue.set(state.visualisation, side, {
        id,
        requirements,
        type
      });
    },
    setSelectedNodes(state, selectedNodes) {
      selectedNodes.forEach(n => {
        if (!state.selectedNodes.includes(n)) {
          state.selectedNodes.push(n);
        }
      });
    },
    addSelectedNode(state, node) {
      state.selectedNodes.push(node);
    },
    clearSelectedNodes(state) {
      state.selectedNodes = [];
    },
    setDegree(state, degree) {
      state.degree = degree;
    },
    setMaxDegree(state, degree) {
      state.maxDegree = degree;
    },
    addZoomSelectionNode(state, node) {
      state.zoomSelectionNodes.push(node);
    },
    clearZoomSelectionNodes(state) {
      state.zoomSelectionNodes = [];
    },
    setZoomSelectionNodes(state, nodes) {
      state.zoomSelectionNodes = [...nodes];
    },
    addZoomSelectionLink(state, link) {
      state.zoomSelectionLinks.push(link);
    },
    clearZoomSelectionLinks(state) {
      state.zoomSelectionLinks = [];
    },
    setZoomSelectionLinks(state, links) {
      state.zoomSelectionLinks = links;
    },
    setZoomed(state) {
      state.isZoomed = !state.isZoomed;
    },
    toggleDescending(state) {
      state.descending = !state.descending;
    }
  },
  actions: {
    bindDatasetHistoryRef: firestoreAction(context => {
      return context.bindFirestoreRef(
        "datasetHistory",
        db.collection("datasets").orderBy("timestamp", "desc")
      );
    }),
    bindInsightsRef: firestoreAction(context => {
      return context.bindFirestoreRef(
        "insights",
        db.collection("insights").orderBy("timestamp", "desc")
      );
    }),
    async uploadDataSets({ commit, state }) {
      commit(
        "setLoadingMessage",
        `We are uploading your ${
          state.files.length > 1 ? "files. They" : "file. It"
        } will appear below shortly.`
      );

      const promises = state.files.map(async file => {
        const newDoc = await db.collection("datasets").add({
          filename: file.name,
          isParsed: false,
          timestamp: firebase.firestore.FieldValue.serverTimestamp()
        });
        return await storage
          .ref(`datasets/raw_${newDoc.id}.csv`)
          .put(file.file);
      });

      await Promise.all(promises);
      state.files = [];
      commit("setLoadingMessage", null);
    },
    async getData({ commit, state }) {
      commit(
        "setLoadingMessage",
        `We are fetching the parsed data. On a slow network connection this might take a while...`
      );

      const left = {
        req: state.visualisation.left.requirements,
        type: state.visualisation.left.type
      };
      const right = {
        req: state.visualisation.right.requirements,
        type: state.visualisation.right.type
      };

      const promises = [left, right].map(async requiredData => {
        const url = await storage
          .ref(
            `datasets/${requiredData.req}_${
              requiredData.type
                ? requiredData.type + "_" + !state.descending + "_"
                : ""
            }${state.currentDataset.id}.json`
          )
          .getDownloadURL();
        const file = await axios.get(url);
        return commit("setCurrentDatasetData", {
          dataType: requiredData.req,
          data: file.data
        });
      });

      await Promise.all(promises);

      commit("setLoadingMessage", null);
    },
    async setVisType({ commit }, { side, id, requirements, type }) {
      commit("setVisType", { side, id, requirements, type });
    },
    async saveInsight(
      { state },
      { matrixImage, graphImage, author, text, title }
    ) {
      const newDoc = await db.collection("insights").add({
        title,
        author,
        text,
        dataset: state.currentDataset.id,
        datasetName: state.currentDataset.name,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
      });

      const promises = [];

      promises.push(
        await storage
          .ref(`insights/matrix_${newDoc.id}.png`)
          .putString(matrixImage, "data_url", { contentType: "image/png" })
      );
      promises.push(
        await storage
          .ref(`insights/graph_${newDoc.id}.png`)
          .putString(graphImage, "data_url", { contentType: "image/png" })
      );

      return Promise.all(promises);
    }
  }
});
