import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import store from "@/store";

Vue.use(Router);

const visualisation = () => import("@/views/Visualisation.vue");
const upload = () => import("@/views/Upload.vue");
const viewInsight = () => import("@/views/ViewInsight.vue");
const allInsights = () => import("@/views/Insights.vue");

const router = new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/home",
      component: Home
    },
    {
      path: "/upload",
      name: "upload",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: upload
    },
    {
      path: "/visualisation",
      name: "visualisation",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: visualisation
    },
    {
      path: "/insights",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: allInsights
    },
    {
      path: "/insights/:id",
      name: "viewInsigt",
      props: true,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: viewInsight
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (store.state.waitForStore) {
    // Hold the request, until the Storage is complete.
    store._vm.$root.$on("storageReady", () => {
      next();
    });
  } else {
    next();
  }
});

export default router;
